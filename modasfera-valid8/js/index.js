$(document).ready(function() {
	$("#enviar").click(function(){
		$("input").isValid();
	})

	$("#nome").valid8('Digite o seu nome');

 	$('#email').valid8({
		'regularExpressions': [
			{
				expression: /^.+$/, 
				errormessage: 'Digite um e-mail'
			},		
			{
				expression: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 
				errormessage: 'Digite um e-mail válido'
			}
		]
	});

 	$("#preco").valid8({
 		'regularExpressions': [
			{
				expression: /^.+$/, 
				errormessage: 'Digite um preço'
			}
 		],
 		'jsFunctions': [
 			{
 				function: function(valor) {
 					if (/^\d{1,}\,\d{2}$/.test(valor) || /^\d+$/.test(valor)) {
	 					return {valid: true};
 					} else {
 						return {valid: false, message: 'Digite um preço válido'};
 					}
 				},
 				values: function() {
 					return $("#preco").val();
 				}
 			}
 		]
 	});

 	$("#comprimento").valid8({
 		'regularExpressions': [
			{
				expression: /^.+$/, 
				errormessage: 'Digite um comprimento'
			}
 		],
 		'jsFunctions': [
 			{
 				function: function(valor) {
 					if (/^\d{1,}\,\d{1,2}$/.test(valor) || /^\d+$/.test(valor)) {
 						return {valid: true };
 					} else {
 						return {valid: false, message: 'Digite um comprimento válido'};
 					}
 				},
 				values: function() {
 					return $("#comprimento").val();
 				}
 			}
 		]
 	});

	$('#twitter').valid8({
		'regularExpressions': [
			{
				expression: /^.+$/, 
				errormessage: 'Digite seu usuario do twitter'
			},
			{
				expression: /^@[A-Za-z0-9]{1,15}$/, 
				errormessage: 'Digite um usuario do twitter válido'	
			}
		]
	});

 	$('#idade').valid8({
		'regularExpressions': [
			{
				expression: /^.+$/, 
				errormessage: 'Digite sua idade'
			},
			{
				expression: /^[0-9]+$/, 
				errormessage: 'Digite somente numeros'	
			}
		],
		'jsFunctions': [
			{
				function: function(idade) {
					idade = parseInt(idade);
					if (idade < 18) {
						return { valid: false, message: 'Somente maiores de idade' }
					} else {
						return { valid: true}
					}
				},
				values: function() {
					return $("#idade").val();
				}
			}
		]
	});	

});